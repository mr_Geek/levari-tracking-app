import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Hour } from '../models/hour';
import { Company } from '../models/company';
import { Response } from '../models/response';

import { Global } from '../services/global';
/*
  Generated class for the TrackingHours provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TrackingHours {

	API_URL: string;

	constructor(
		public http: Http, 
		private global: Global
	) {
		this.API_URL = this.global.getURL()
	}

  load(token): Observable<Hour[]> {
		let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})
		let options = new RequestOptions({ headers: headers })
		
  	return this.http.get(`${this.API_URL}/getHours`, options)
  									.map(res => <Hour[]>res.json())
	}
	
	loadWithoutAuthenticating(): Observable<Hour[]> {
		let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': 'bypass-login'
  	})
		let options = new RequestOptions({ headers: headers })
		
  	return this.http.get(`${this.API_URL}/getHours`, options)
  									.map(res => <Hour[]>res.json())
  }

	getCompanies(token): Observable<Company[]> {
		let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})
		let options = new RequestOptions({ headers: headers })
  	return this.http.get(`${this.API_URL}/getUserCompanies`, options)
  									.map(res => <Company[]>res.json())
  }

  submitHour(clientName, pricePerHour, details, chargable, priceCustom, totalHours, startTime, endTime, token): Observable<Response> {
  	let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})

  	let options = new RequestOptions({ headers: headers })
    let hourModel = {
      clientName: clientName,
      pricePerHour: pricePerHour,
      numberOfHours: totalHours,
			details: details,
			chargable: chargable,
			priceCustom: priceCustom,
			totalPrice: 0,
			startTime: startTime,
			endTime: endTime
    }
  	return this.http
  			.post(`${this.API_URL}/createHour`, hourModel, options)
  			.map(res => <Response>res.json())
  }

	// submitHour(clientName, pricePerHour, totalHours): Observable<Response> {
	//
	// 	let token = this.storage.get('ionic_auth_3808ed48')
	//
  // 	let headers = new Headers({
  // 		'Content-Type': 'application/json',
	// 		'authorization': token
  // 	})
  // 	let options = new RequestOptions({ headers: headers })
  //   let hourModel = {
  //     clientName: clientName,
  //     pricePerHour: pricePerHour,
  //     numberOfHours: totalHours,
	// 		totalPrice: parseFloat(pricePerHour) * parseFloat(totalHours)
  //   }
  // 	return this.http
  // 			.post(`${this.API_URL}/createHour`, hourModel, options)
  // 			.map(res => <Response>res.json())
  // }

	createCompany(companyName, pricePerHour, token): Observable<Response> {

  	let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})
  	let options = new RequestOptions({ headers: headers })
    let companyModel = {
      companyName: companyName,
      pricePerHour: pricePerHour
    }
  	return this.http
  			.post(`${this.API_URL}/createCompany`, companyModel, options)
  			.map(res => <Response>res.json())
  }

}
