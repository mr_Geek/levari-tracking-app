import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Global } from '../services/global';

import { Company } from '../models/company';

/*
  Generated class for the TrackingHours provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ProfileProvider {

	API_URL: string;

	constructor(
		public http: Http, 
		private global: Global
	) {
		this.API_URL = this.global.getURL()
	}

	getUserCompanies(token): Observable<Company[]> {
		let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})
  	let options = new RequestOptions({ headers: headers })
  	return this.http.get(`${this.API_URL}/getUserCompanies`, options)
  									.map(res => <Company[]>res.json())
  }

	getUserStats(token): Observable<Object> {
		let headers = new Headers({
  		'Content-Type': 'application/json',
			'authentication': token
  	})
  	let options = new RequestOptions({ headers: headers })
  	return this.http.get(`${this.API_URL}/getUserStats`, options)
  									.map(res => <Object>res.json())
  }

}
