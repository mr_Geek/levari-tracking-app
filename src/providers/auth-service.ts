import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { Storage } from "@ionic/storage";

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  constructor(public storage: Storage) {}

  authenticated() {
    this.storage.get('token').then((token) => {
      return tokenNotExpired(null, token)
    })
  }

}
