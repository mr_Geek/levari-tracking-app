import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Headers, Http, RequestOptions } from "@angular/http";
import { JwtHelper, tokenNotExpired } from "angular2-jwt";
import { Storage } from "@ionic/storage";

import { Global } from '../services/global';

/*
  Generated class for the Auth provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Auth {

  API_URL: string;
  user: string

  constructor(public http: Http, 
    private jwtHelper: JwtHelper, 
    private storage: Storage,
    private global: Global
  ) {
    this.API_URL = this.global.getURL()
  }

  login(credentials) {
    let headers = new Headers({
      'Content-Type': 'application/json'
    })

  	return this.http
  			.post(`${this.API_URL}/auth/login`, JSON.stringify(credentials), {headers: headers})
  			.map(res => res.json())
  }

  logout() {
    this.storage.remove('token')
    this.storage.remove('user-id')
    this.user = null
  }

  authSuccess(token) {
    this.storage.set('token', token)
    this.user = this.jwtHelper.decodeToken(token).id
    this.storage.set('user-id', this.user)
  }

  getToken() {
    return this.storage.get('token')
  }

  decodeToken(token) {
    return this.jwtHelper.decodeToken(token)
  }

  checkTokenExp(token) {
    return tokenNotExpired(null, token)
	}

  refreshToken(token) {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'authentication': token
    })
    let options = new RequestOptions({ headers: headers })
    return this.http.get(`${this.API_URL}/auth/refreshToken`, options)
                    .map(res => res.json())
  }


}
