import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { FormsModule } from '@angular/forms';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { JwtHelper } from "angular2-jwt";

import { HomePage } from '../pages/home/home';
import { HourDetailsPage } from '../pages/hour-details/hour-details';
import { AddHourPage } from '../pages/add-hour/add-hour';
import { AddCompanyPage } from '../pages/add-company/add-company';
import { StopWatchPage } from '../pages/stop-watch/stop-watch';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';

import { TimerComponent } from '../components/timer/timer';
import { ExpandableHeaderComponent } from '../components/expandable-header/expandable-header';
import { TextAvatarDirective } from '../components/text-avatar/text-avatar';

import { TimePipe } from './../pipes/time-pipe';
import { ModifyStringPipe } from '../pipes/modify-string';

import { AuthService } from '../providers/auth-service';
import { TrackingHours } from '../providers/tracking-hours';
import { ProfileProvider } from '../providers/profile-provider';
import { Auth } from '../providers/auth';

import { SentryErrorHandler } from '../services/sentry-errorHandler';
import { Global } from '../services/global';

// const cloudSettings: CloudSettings = {
//   'core': {
//     'app_id': '3808ed48'
//   }
// };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HourDetailsPage,
    AddHourPage,
    StopWatchPage,
    TimerComponent,
    AddCompanyPage,
    TabsPage,
    LoginPage,
    ProfilePage,
    TextAvatarDirective,
    ExpandableHeaderComponent,
    TimePipe,
    ModifyStringPipe
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    FormsModule,
    HttpModule,
    BrowserModule,
    // CloudModule.forRoot(cloudSettings),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HourDetailsPage,
    AddHourPage,
    StopWatchPage,
    TimerComponent,
    AddCompanyPage,
    TabsPage,
    LoginPage,
    ProfilePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: SentryErrorHandler},
    TrackingHours,
    Auth,
    JwtHelper,
    AuthService,
    ProfileProvider,
    Global
  ]
})
export class AppModule {}
