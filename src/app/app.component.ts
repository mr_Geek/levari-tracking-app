import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { TabsPage } from '../pages/tabs/tabs';

import { LoginPage } from '../pages/login/login';
// import { Auth } from '@ionic/cloud-angular';
import { tokenNotExpired } from 'angular2-jwt';
import { Storage } from "@ionic/storage";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any;
  // pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public _SplashScreen: SplashScreen,
    public storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this._SplashScreen.hide();
      this.statusBar.styleDefault();

      this.storage.get('token').then((token) => {
        if (tokenNotExpired(null, token)) {
          this.rootPage = TabsPage;
        } else {
          this.rootPage = LoginPage;
        }
      })

    });
  }
}
