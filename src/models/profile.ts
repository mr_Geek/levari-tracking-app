import { Company } from './company';

export interface Profile {
	firstName: string;
	email: string;
	id: number;
  companies: Company[];
  username: string;
}
