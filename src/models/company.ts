export interface Company {
	companyName: string;
	pricePerHour: string;
	id: number;
}
