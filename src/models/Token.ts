export interface Token {
	email: string;
  username: string;
  firstName: string;
  id: string;
}
