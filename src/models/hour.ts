export interface Hour {
	clientName: string;
	pricePerHour: string;
	numberOfHours: string;
	totalPrice: string;
	code: string;
	data: any;
	message: string;
}