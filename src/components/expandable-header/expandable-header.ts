import { Component, Input, ElementRef, Renderer } from '@angular/core';

/*
  Generated class for the ExpandableHeader component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'expandable-header',
  templateUrl: 'expandable-header.html'
})
export class ExpandableHeaderComponent {

  @Input('scrollArea') scrollArea: any;
  @Input('headerHeight') headerHeight: number;


  newHeaderHeight: any;
  text: string;

  constructor(public element: ElementRef, public renderer: Renderer) {

  }

  ngOnInit(){
    // if (!this.scrollArea._scroll.enabled) {
    //   this.scrollArea.enableScrollListener()
    // }
    this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px')

    this.scrollArea.ionScroll.subscribe((ev) => {
      this.resizeHeader(ev)
    })
  }

  resizeHeader(ev) {
    ev.domWrite(() => {
      // scrollTop is the distance the user has scrolled from the top of the content area)
      this.newHeaderHeight = this.headerHeight - ev.scrollTop

      if (this.newHeaderHeight < 0) {
        this.newHeaderHeight = 0
      }
      this.renderer.setElementStyle(this.element.nativeElement, 'height', this.newHeaderHeight + 'px')

      // This part to hide every thing in the header itself
      for (let headerElement of this.element.nativeElement.children) {
        let totalHeight = headerElement.offsetTop + headerElement.clientHeight

        if (totalHeight > this.newHeaderHeight && !headerElement.isHidden) {
          headerElement.isHidden = true
          this.renderer.setElementStyle(headerElement, 'opacity', '0')
        } else if (totalHeight <= this.newHeaderHeight && headerElement.isHidden) {
          headerElement.isHidden = false
          this.renderer.setElementStyle(headerElement, 'opacity', '1')
        }
      }

    })
  }




















}
