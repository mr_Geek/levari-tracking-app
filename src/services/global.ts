export class Global {
	url: string = 'https://lawyers-platform.herokuapp.com'
	// url: string = 'https://levari-tracking.herokuapp.com'

	public getURL () {
		return this.url
	}
}