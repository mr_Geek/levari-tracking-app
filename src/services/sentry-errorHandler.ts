import {IonicErrorHandler } from 'ionic-angular';
import Raven from 'raven-js';

Raven.config('https://eebeb217c4ad4ea79681ce77149462e5@sentry.io/210304')
		 .install()

export class SentryErrorHandler extends IonicErrorHandler {

	handleError (error) {
		super.handleError(error)

		try {
			Raven.captureException(error.originalError || error)
		} catch(e) {
			console.log(e)
		}

	}


}