import { Injectable, Pipe } from '@angular/core';
import * as moment from 'moment';

/*
  Generated class for the TimePipe pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'timePipe'
})
@Injectable()
export class TimePipe {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value, args) {
    return moment(value, 'MM-DD-YYYY hh:mm:ssA').format('h:m:sA')
  }
}
