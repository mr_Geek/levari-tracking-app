import { Injectable, Pipe } from '@angular/core';

/*
  Generated class for the TimePipe pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'modifyString'
})
@Injectable()
export class ModifyStringPipe {

  /*
    Takes a value and makes it lowercase.
   */
  transform(value, args) {
    return value ? value.length > 150 ? value.substr(0, 150) + '...' : value : ''
	}
	
	
}
