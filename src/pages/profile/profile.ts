import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { ProfileProvider } from '../../providers/profile-provider';
import { Auth } from '../../providers/auth';
import { Company } from '../../models/company';
import { LoginPage } from '../login/login';
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  segment: string = "clients";
  companies: Company[];
  user_name: string = '';
  email: string = '';

  /* Stastics Variabls */
  totalClients: number;
  totalPrice: number;
  totalHours: number;
  totalPerClient: any;
  /* Stastics Variabls */


  constructor(
    public  navCtrl: NavController,
    public  navParams: NavParams,
    public  auth: Auth,
    public  profileProvider: ProfileProvider,
    public  storage: Storage,
    public  toastCtrl: ToastController) {}

  ngOnInit() {
    this.storage.get('token').then((token) => {
      const tokenDecoded = this.auth.decodeToken(token)
      this.user_name = tokenDecoded.firstName
      this.email = tokenDecoded.email


      this.profileProvider.getUserCompanies(token).subscribe(companies => {
    		this.companies = companies['data']
    	},
      err => {
        let toast = this.toastCtrl.create({
          message: 'Error occurred',
          duration: 3000,
          showCloseButton: true,
          closeButtonText: 'Ok'
        })
        toast.present()
      })

      this.profileProvider.getUserStats(token).subscribe(stats => {
        this.totalClients = stats['data'].total_clients
        this.totalPrice = stats['data'].total_price
        this.totalHours = stats['data'].total_hours
        this.totalPerClient = stats['data'].total_per_client
      })

    })
  }

  initialLoad(refresher) {
    this.storage.get('token').then((token) => {
      /* Check if token has expired, if so refresh it before sending request */
      if (!this.auth.checkTokenExp(token)) {
        this.auth.refreshToken(token).subscribe(data => {
          // Token refreshed, now save it
          this.storage.set('token', data.token)
          // Send the request now..
          this.profileProvider.getUserCompanies(data.token).subscribe(companies => {
        		this.companies = companies['data'];
            refresher.complete();
        	},
          err => {
            let toast = this.toastCtrl.create({
              message: 'Error occurred',
              duration: 3000,
              showCloseButton: true,
              closeButtonText: 'Ok'
            })
            toast.present()
          })
          this.profileProvider.getUserStats(data.token).subscribe(stats => {
            this.totalClients = stats['data'].total_clients
            this.totalPrice = stats['data'].total_price
            this.totalHours = stats['data'].total_hours
            this.totalPerClient = stats['data'].total_per_client
          })
        })
      } else {
        this.profileProvider.getUserCompanies(token).subscribe(companies => {
      		this.companies = companies['data'];
          refresher.complete();
      	},
        err => {
          let toast = this.toastCtrl.create({
            message: 'Error occurred',
            duration: 3000,
            showCloseButton: true,
            closeButtonText: 'Ok'
          })
          toast.present()
        })
        this.profileProvider.getUserStats(token).subscribe(stats => {
          this.totalClients = stats['data'].total_clients
          this.totalPrice = stats['data'].total_price
          this.totalHours = stats['data'].total_hours
          this.totalPerClient = stats['data'].total_per_client
        })
      }

    })
  }

  getRootNav(nav: NavController): NavController {
    let root = nav;
    while (root.parent != null) {
      root = root.parent;
    }
    return root;
  }

  logout() {
    this.auth.logout()
    let rootNav = this.getRootNav(this.navCtrl)
    rootNav.setRoot(LoginPage)
  }


}
