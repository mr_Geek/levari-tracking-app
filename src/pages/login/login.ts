import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

// import { Auth, User } from '@ionic/cloud-angular';
import { TabsPage } from "../tabs/tabs";
import { Auth } from '../../providers/auth';
import { Storage } from "@ionic/storage";

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string = '';
  password: string = '';
  firstName: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: Auth, public alertCtrl: AlertController, public loadingCtrl:LoadingController, public storage: Storage) {
  }

  ionViewDidLoad() {

  }

  doLogin() {
    console.log('Process Login...')

    if (this.email === '' || this.password === ''){
      let alert = this.alertCtrl.create({
        title: 'Login Error',
        subTitle: 'All fields are required!',
        buttons: ['OK']
      })
      alert.present()
      return
    }

    let loader = this.loadingCtrl.create({
      content: 'Logging in...'
    })
    loader.present()
    
    this.auth.login({email: this.email, password: this.password})
             .subscribe(
               data => {
                 this.auth.authSuccess(data.token)
                 loader.dismissAll()
                 this.navCtrl.setRoot(TabsPage)
               },
               err => {
                 console.log(err)
                 let alert = this.alertCtrl.create({
                   title: 'Login Error',
                   subTitle: err._body.message ? JSON.parse(err._body).message : 'Unknown error occurred while connecting to our network, Please try again later',
                   buttons: ['OK']
                 })
                 alert.present()
                 loader.dismissAll()
               }
             )
  }

  // doLogin() {
  //   console.log('Process Login...')
  //
  //   if (this.email === '' || this.password === ''){
  //     let alert = this.alertCtrl.create({
  //       title: 'Login Error',
  //       subTitle: 'All fields are required!',
  //       buttons: ['OK']
  //     })
  //     alert.present()
  //     return
  //   }
  //
  //   let loader = this.loadingCtrl.create({
  //     content: 'Logging in...'
  //   })
  //   loader.present()
  //
  //   this.auth.login('custom', {email: this.email, password: this.password})
  //            .then(() => {
  //              console.log('Ok i guess!')
  //              loader.dismissAll()
  //
  //              this.navCtrl.setRoot(TabsPage)
  //            }, (err) => {
  //              loader.dismissAll()
  //              console.log(err.message)
  //
  //              let errors = ''
  //              if (err.message === 'UNPROCESSABLE ENTITY') {
  //                errors += 'Email is not valid. <br/>'
  //              }
  //              if (err.message === 'UNAUTHORIZED') {
  //                errors += 'Password is required. <br/>'
  //              }
  //              if (errors === ''){
  //                errors = err
  //              }
  //              let alert = this.alertCtrl.create({
  //                title: 'Login Error',
  //                subTitle: errors,
  //                buttons: ['OK']
  //              })
  //              alert.present()
  //            })
  // }















}
