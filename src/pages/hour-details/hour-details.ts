import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// import { TimePipe } from '../../pipes/time-pipe';

/*
Generated class for the HourDetails page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
  selector: 'page-hour-details',
  templateUrl: 'hour-details.html'
})
export class HourDetailsPage {

  hour: any;

  // .replace(/\n/g, "<br />")
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let hour = navParams.get('hour')
    hour.details = hour.details.replace(/\n/g, "<br />")
    this.hour = hour;
  }


}
