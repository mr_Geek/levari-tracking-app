import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

import { TrackingHours } from '../../providers/tracking-hours';
import { TimerComponent } from '../../components/timer/timer';
import { Storage } from '@ionic/storage';
import { Auth } from '../../providers/auth';
import * as moment from 'moment';

/*
  Generated class for the StopWatch page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-stop-watch',
  templateUrl: 'stop-watch.html'
})
export class StopWatchPage {

  @ViewChild(TimerComponent) timer: TimerComponent;

	clientName: string;
	pricePerHour: string;
  numberOfHours: string;
  details: string;
	response: any;
  totalHours: string;
  totalPrice: number;
  chargable: boolean;
  priceCustom: boolean;

  startTime: string;
  endTime: string;

  constructor(
    public  navCtrl: NavController,
    public  navParams: NavParams,
    private trackingHours: TrackingHours,
    public  toastCtrl: ToastController,
    private storage: Storage,
    public  auth: Auth) {
  	this.clientName = navParams.get('clientName');
    this.details = navParams.get('details');
    this.pricePerHour = navParams.get('pricePerHour');
    this.chargable = navParams.get('chargable');
  	this.priceCustom = navParams.get('priceCustom');
    this.numberOfHours = '0001';
  }

  ngOnInit() {
    setTimeout( () => {
      this.startTime = moment().format('x');
      this.timer.startTimer();
    }, 1000);
  }

  save() {
    this.endTime = moment().format('x')
    this.timer.pauseTimer()

    let token = this.storage.get('token')

    if (token === null) {
      let toast = this.toastCtrl.create({
        message: 'Please login again..',
        duration: 3000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      toast.present()
    } else {

      var sec_num  = this.timer.getElapsedTime()
      var hours    = Math.floor(sec_num / 3600)
      var minutes  = Math.floor((sec_num - (hours * 3600)) / 60)

      this.totalHours = this.timer.getSecondsAsDigitalClock(this.timer.timer.secondsRemaining)
      //Just to be shown to the user
      this.totalPrice = Math.floor( ( ( hours * 60 + minutes ) / 60 ) * parseFloat(this.pricePerHour) )

      this.storage.get('token').then((token) => {
        /* Check if token has expired, if so refresh it before sending request */
        if (!this.auth.checkTokenExp(token)) {
          this.auth.refreshToken(token).subscribe(data => {
            //Token Refreshed, now save it
            this.storage.set('token', data.token)
            // Send the request now...
            this.trackingHours.submitHour(
                                          this.clientName,
                                          this.pricePerHour,
                                          this.details,
                                          this.chargable,
                                          this.priceCustom,
                                          hours,
                                          this.startTime,
                                          this.endTime,
                                          data.token)
                              .subscribe(data => {
                                let toast = this.toastCtrl.create({
                                  message: data.message,
                                  duration: 3000,
                                  showCloseButton: true,
                                  closeButtonText: 'Ok'
                                })
                                toast.present()
                              },
                              err => {
                                let toast = this.toastCtrl.create({
                                  message: err,
                                  duration: 3000,
                                  showCloseButton: true,
                                  closeButtonText: 'Ok'
                                })
                                toast.present()
                              })
          }, err => {
            let toast = this.toastCtrl.create({
              message: 'Could not refresh the token, please logout and login again.',
              duration: 3000,
              showCloseButton: true,
              closeButtonText: 'Ok'
            })
            toast.present()
          })
        } else  {
          /* Token is valid, send request */
          this.trackingHours.submitHour(
                                        this.clientName,
                                        this.pricePerHour,
                                        this.details,
                                        this.chargable,
                                        this.priceCustom,
                                        hours,
                                        this.startTime,
                                        this.endTime,
                                        token)
                            .subscribe(data => {
                              let toast = this.toastCtrl.create({
                                message: data.message,
                                duration: 3000,
                                showCloseButton: true,
                                closeButtonText: 'Ok'
                              })
                              toast.present()
                            },
                            err => {
                              let toast = this.toastCtrl.create({
                                message: err,
                                duration: 3000,
                                showCloseButton: true,
                                closeButtonText: 'Ok'
                              })
                              toast.present()
                            })
        }
      })
    }



  }







}
