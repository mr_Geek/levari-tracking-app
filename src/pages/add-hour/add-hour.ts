import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

// import { Response } from '../../models/response';
import { TrackingHours } from '../../providers/tracking-hours';
import { StopWatchPage } from '../stop-watch/stop-watch'
import { Company } from '../../models/company';
import { Auth } from '../../providers/auth';


/*
  Generated class for the AddHour page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-hour',
  templateUrl: 'add-hour.html'
})
export class AddHourPage {

	clientName: string = '';
	pricePerHour: string = '';
  details: string = '';
  companyID: string = '';
  companies: Company[];
  chargable: boolean = true;
  priceCustom: boolean = false;
  chargableText: string = 'Chargable';
  priceText: string = 'Default Price';

  constructor(
    public navCtrl: NavController, 
    private trackingHours: TrackingHours, 
    public toastCtrl: ToastController,
    public storage: Storage,
    private auth: Auth
  ) {
  }

  ngOnInit(){
    // this.storage.get('token').then((token) => {
    //   /* Check if token has expired, if so refresh it before sending request */
      
    //   if (!this.auth.checkTokenExp(token)) {
    //     this.auth.refreshToken(token).subscribe(data => {
    //       this.storage.set('token', data.token)
    //       this.trackingHours.getCompanies(data.token).subscribe(companies => {
    //         if (companies['data']) {
    //           this.companies = companies['data']
    //         } else {
    //           this.companies = []
    //         }
    //       })
    //     })
    //   } else {
    //     this.trackingHours.getCompanies(token).subscribe(companies => {
    //       if (companies['data']) {
    //         this.companies = companies['data']
    //       } else {
    //         this.companies = []
    //       }
    //     })
    //   }
    // })

    // this.trackingHours.getCompanies().subscribe(companies => {
    //   if (companies['data']) {
    //     this.companies = companies['data']
    //   } else {
    //     this.companies = []
    //   }
  	// })
  }

  ionViewDidEnter() {
    this.pricePerHour = ''
    // this.trackingHours.getCompanies().subscribe(companies => {
    //   if (companies['data']) {
    //     this.companies = companies['data']
    //   } else {
    //     this.companies = []
    //   }
    // })
    this.storage.get('token').then((token) => {
      /* Check if token has expired, if so refresh it before sending request */
      
      if (!this.auth.checkTokenExp(token)) {
        this.auth.refreshToken(token).subscribe(data => {
          this.storage.set('token', data.token)
          this.trackingHours.getCompanies(data.token).subscribe(companies => {
            if (companies['data']) {
              this.companies = companies['data']
            } else {
              this.companies = []
            }
          })
        })
      } else {
        this.trackingHours.getCompanies(token).subscribe(companies => {
          if (companies['data']) {
            this.companies = companies['data']
          } else {
            this.companies = []
          }
        })
      }
    })

  }

  makeNotChargable() {
    this.chargable = this.chargable ? false : true
    this.chargableText = this.chargable ? 'Chargable' : 'Not Chargable'
  }

  makePriceDefault() {
    this.priceCustom = this.priceCustom ? false : true
    this.priceText = this.priceCustom ? 'Custom Price' : 'Default Price'
  }


  submit() {
    let flag = 'default'
    if(this.chargable && this.pricePerHour && this.pricePerHour !== '') {
      flag = 'custom'
    }

    for (var i=0 ; i<this.companies.length ; i++){
      if ((this.companies[i].id).toString() == this.companyID){
        this.clientName = this.companies[i].companyName;
        if (flag === 'default') {
          this.pricePerHour = this.companies[i].pricePerHour;
        }
        break;
      }
    }

    console.log(this.priceCustom)
    console.log(this.chargable)

    if (this.clientName && this.pricePerHour && this.details) {
      this.navCtrl.push(StopWatchPage, {
        clientName: this.clientName,
        pricePerHour: this.pricePerHour,
        details: this.details,
        chargable: this.chargable,
        priceCustom: this.priceCustom
      })
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please fill in all the fields to start tracking',
        duration: 3000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      toast.present()
    }

  }


}
