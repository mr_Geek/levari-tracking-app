import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Http} from '@angular/http';

import { Storage } from '@ionic/storage';
import { TrackingHours } from '../../providers/tracking-hours';
// import { StopWatchPage } from '../stop-watch/stop-watch'
import { Auth } from '../../providers/auth';

/*
  Generated class for the AddCompany page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-company',
  templateUrl: 'add-company.html'
})
export class AddCompanyPage {

  companyName: string = '';
	pricePerHour: string = '';

  constructor(
    public  navCtrl: NavController,
    public  navParams: NavParams,
    private trackingHours: TrackingHours,
    private http: Http,
    public  toastCtrl: ToastController,
    public  storage: Storage,
    private auth: Auth) {
  }

  submit() {
    if (this.companyName !== '' && this.pricePerHour !== ''){
      this.storage.get('token').then((token) => {
        /* Check if token has expired, if so refresh it before sending request */
        if (!this.auth.checkTokenExp(token)) {
          this.auth.refreshToken(token).subscribe(data => {
            // Token refreshed, now save it
            this.storage.set('token', data.token)
            // Send the request now..
            this.trackingHours.createCompany(this.companyName, this.pricePerHour, data.token)
                              .subscribe(data => {
                                if (data.data === false){
                                  let toast = this.toastCtrl.create({
                                    message: '(' + data['code'] + ') ' + data['message'],
                                    duration: 3000,
                                    showCloseButton: true,
                                    closeButtonText: 'Ok'
                                  })
                                  toast.present()
                                } else {
                                  let toast = this.toastCtrl.create({
                                    message: 'Client was successfully created',
                                    duration: 3000,
                                    showCloseButton: true,
                                    closeButtonText: 'Ok'
                                  })
                                  toast.present()
                                }
                              },
                              err => {
                                let toast = this.toastCtrl.create({
                                  message: 'Error occurred',
                                  duration: 3000,
                                  showCloseButton: true,
                                  closeButtonText: 'Ok'
                                })
                                toast.present()
                              })
          })
        } else {
          this.trackingHours.createCompany(this.companyName, this.pricePerHour, token)
                            .subscribe(data => {
                              if (data.data === false){
                                let toast = this.toastCtrl.create({
                                  message: '(' + data['code'] + ') ' + data['message'],
                                  duration: 3000,
                                  showCloseButton: true,
                                  closeButtonText: 'Ok'
                                })
                                toast.present()
                              } else {
                                let toast = this.toastCtrl.create({
                                  message: 'Client was successfully created',
                                  duration: 3000,
                                  showCloseButton: true,
                                  closeButtonText: 'Ok'
                                })
                                toast.present()
                              }
                            },
                            err => {
                              let toast = this.toastCtrl.create({
                                message: 'Error occurred',
                                duration: 3000,
                                showCloseButton: true,
                                closeButtonText: 'Ok'
                              })
                              toast.present()
                            })
        }
      })
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please fill in all fields before adding new client',
        duration: 3000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      toast.present()
    }

  }

}
