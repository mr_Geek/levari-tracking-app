import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AddHourPage } from '../add-hour/add-hour';
import { HomePage } from '../home/home';
import { AddCompanyPage } from '../add-company/add-company';
import { ProfilePage } from '../profile/profile';



/*
  Generated class for the Tabs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AddHourPage;
  tab3Root = AddCompanyPage;
  tab4Root = ProfilePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

}
