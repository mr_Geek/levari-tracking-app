import { Component } from '@angular/core';
import { IonicApp, NavController, App, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from "@ionic/storage";

import { Hour } from '../../models/hour';
import { TrackingHours } from '../../providers/tracking-hours';

import { HourDetailsPage } from '../hour-details/hour-details';
import { AddHourPage } from '../add-hour/add-hour';
import { LoginPage } from '../login/login';
import { Auth } from '../../providers/auth';
import { Platform } from 'ionic-angular';

// import { TimePipe } from '../../pipes/time-pipe';
// import { Auth, User } from '@ionic/cloud-angular';

import { ModifyStringPipe } from '../../pipes/modify-string';



/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  hours: Hour[];
  lastBack: any;
  allowClose: boolean = false;
  spinnerFlag: boolean = false;

  constructor(
    public   navCtrl: NavController,
    private  trackingHours: TrackingHours,
    public   platform: Platform,
    public   ionicApp: IonicApp,
    public   toastCtrl: ToastController,
    public   app: App,
    public   auth: Auth,
    public   loadingController: LoadingController,
    public   storage: Storage) {

    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        const overlay = this.app._appRoot._overlayPortal.getActive()
        const nav = this.app.getActiveNav()
        const closeDelay = 2000
        const spamDelay = 500

        if (overlay && overlay.dismiss) {
          overlay.dismiss()

        } else if (nav.canGoBack()) {
          nav.pop()

        } else if (Date.now() - this.lastBack > spamDelay && !this.allowClose) {
          this.allowClose = true
          let toast = toastCtrl.create({
            message: 'Tap back again to exit.',
            duration: closeDelay,
            dismissOnPageChange: true
          })
          toast.onDidDismiss(() => {
            this.allowClose = false
          })
          toast.present()
        } else if (Date.now() - this.lastBack < spamDelay && this.allowClose) {
          this.platform.exitApp()
        }
        this.lastBack = Date.now()
      })
    })
  }

  ngOnInit(){
    let authenticatingLoad = this.loadingController.create({
      content: 'Getting things ready, please wait',
      spinner: 'dots'
    })
    authenticatingLoad.present()
    /* Timeout created to give the token a chance to be saved in order to retrieve it */
    this.createTimeOut(2000)
        .then(() => {
          let fetchingLoad = this.loadingController.create({
            content: 'Fetching data.. hold still',
            spinner: 'dots'
          })
          fetchingLoad.present()

          this.storage.get('token').then( (token) => {
            if (!this.auth.checkTokenExp(token)) {
              fetchingLoad.dismiss()
              this.toastCtrl.create({
                message: 'Session expired, please login again..',
                duration: 3000,
                showCloseButton: true,
                closeButtonText: 'Ok'
              }).present()
            } else {
              this.trackingHours.load(token).subscribe(hours => {
                console.log('Hours: ')
                console.log(hours)
                this.hours = hours;
                fetchingLoad.dismiss()
              }, err => {
                fetchingLoad.dismiss()
                this.toastCtrl.create({
                  message: 'Unable to connect to the server, please check your network and try again.',
                  duration: 3000, 
                  showCloseButton: true,
                  closeButtonText: 'Ok'
                }).present()
                console.log(err)
              })
            }
          })
          authenticatingLoad.dismiss()
        })
  }

  initialLoad(refresher) {
    this.storage.get('token').then( (token) => {
      /* Check if token has expired, if so refresh it before sending request */
      if (!this.auth.checkTokenExp(token)) {
        this.toastCtrl.create({
          message: 'Session expired, please login again..',
          duration: 3000,
          showCloseButton: true,
          closeButtonText: 'Ok'
        }).present()
        // this.auth.logout()
      } else {
        this.trackingHours.load(token).subscribe(hours => {
          this.hours = hours;
        })
      }
    })
    refresher.complete()
  }

  goToDetails(hour: any) {
    this.navCtrl.push(HourDetailsPage, {hour});
  }

  goToAddHour() {
    this.navCtrl.push(AddHourPage);
  }

  getRootNav(nav: NavController): NavController {
    let root = nav;
    while (root.parent != null) {
      root = root.parent;
    }
    return root;
  }

  logout() {
    this.auth.logout()
    let rootNav = this.getRootNav(this.navCtrl)
    rootNav.setRoot(LoginPage)
  }

  createTimeOut(timeout) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(null), timeout)
    })
  }

  

}
